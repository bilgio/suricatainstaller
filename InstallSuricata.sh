#!/bin/bash

###############################################################
#
# BilgiO Suricata Installation Script
#
# This file need the configuration file named ConfigSuricata.cfg
#
# BilgiO - LinuxAkademi 2015
#
###############################################################

# Variables
OSRelease=$(cat /etc/redhat-release)

# Functions

check_if_command_succeed () { 
        if [ $? -ne 0 ]; then
            echo "Something went wrong! Terminating..."
            exit 1
		else
			echo "Done! Continue..."
        fi
}

CONFFILE=./ConfigSuricata.cfg

echo "Welcome to BilgiO Suricata Installation Script"
echo "Reading Configuration From Config File"

source 	$CONFFILE

# Checking Configuration File Section
echo "Checking configuration file .. Please wait"

curl -s --head $UrlPfring | head -n 1 | grep "HTTP/1.[01] [23].." > /dev/null && echo "Pfring Url OK "
curl -s --head $UrlSuricata | head -n 1 | grep "HTTP/1.[01] [23].." > /dev/null && echo "Suricata Url OK "
curl -s --head $UrlOinkmaster | head -n 1 | grep "HTTP/1.[01] [23].." > /dev/null && echo "Oinkmaster Url OK "

# Check if the Files Directory is in right place
ls -l $FilesDir

check_if_command_succeed


# Check if there is an available interface
ifconfig -a | grep $Interface > /dev/null
check_if_command_succeed


# Check if there is an avaliable kernel update it and reboot the system
yum list updates | egrep '^kernel*'
if [ $? -ne 0 ]
then
	echo "There is no new kernel installation will be continue.."
else
	echo "There are available new kernel !!"
	echo "To Install Suricata successfully" 
	echo "Firstly kernel update should be done"
	echo "Update kernel and reboot the system"
	echo "THEN YOU SHOULD RESTART THIS SCRIPT AGAIN !!"
	read -p "OKAY I GOT IT [ Y / N ] ?" -n 1 -r

	if [[ ! $REPLY =~ ^[Yy]$ ]]
	then
		echo "Exiting from script !!!"
		exit 1
	else
		yum update -y kernel*
		check_if_command_succeed

		echo "Rebooting the system"
		/sbin/reboot
	fi
fi


# Install necessary packages

echo "Adding New Repos"
rpm -Uvh $UrlRepoFedora
rpm -Uvh $UrlRepoForge

echo "Updating Repolist"
yum repolist


if [[ $OSRelease == *"CentOS Linux release 7."* ]]; then
	yum install -y net-tools vim tcpdump wget kernel-headers kernel-devel \
	libpcap libpcap-devel openssl-devel libnet libnet-devel pcre numactl \
	numactl-devel vim-enhanced wget kernel-uek-headers kernel-uek-devel kernel-headers \
	flex bison gcc gcc-c++ make kernel-devel man man-pages perf iptraf libcap-ng-devel \
	file-devel tcpdump libnetfilter_queue libnetfilter_queue-devel libnfnetlink \
	libnfnetlink-devel zlib-devel zlib jansson jansson-devel libpcap libpcap-devel libnet \
	libnet-devel pcre pcre-devel gcc gcc-c++ automake autoconf libtool make libyaml libyaml-devel \
	zlib zlib-devel openssh-devel ethtool svn --skip-broken
else
	yum install -y vim tcpdump wget kernel-headers kernel-devel \
	libpcap libpcap-devel openssl-devel libnet libnet-devel pcre numactl \
	numactl-devel vim-enhanced wget kernel-uek-headers kernel-uek-devel kernel-headers \
	flex bison gcc gcc-c++ make kernel-devel man man-pages perf iptraf libcap-ng-devel \
	file-devel tcpdump ngrep libnetfilter_queue libnetfilter_queue-devel libnfnetlink \
	libnfnetlink-devel zlib-devel zlib jansson jansson-devel libpcap libpcap-devel libnet \
	libnet-devel pcre pcre-devel gcc gcc-c++ automake autoconf libtool make libyaml libyaml-devel \
	zlib zlib-devel openssh-devel ethtool svn --skip-broken
fi

# Creating Temprory Installation Directory
mkdir -p $Dir || echo "Couldn't create installation directory !!!"

cd $Dir

# UpdateTheSystem Section
if [ "$UpdateSystem" == "YES" ]
then
	echo "Updating the System"
	yum update -y
	if [ $? -ne 0 ] 
	then
		echo "System Update has been failed"
		exit 1
	fi
else
	echo "Update System is NOT Selected"
fi

# Iptables Section
if [ "$DisableIptables" == "YES" ]
then
	if [[ $OSRelease == *"CentOS Linux release 7."* ]]; then
		systemctl disable firewalld && systemctl stop firewalld
		check_if_command_succeed
	else
		echo "Disabling Iptables"
		/etc/init.d/iptables stop && /etc/init.d/ip6tables stop
		chkconfig iptables off && chkconfig ip6tables off
      	check_if_command_succeed
	fi
else
	echo "Disable Iptables is NOT Selected"
fi

# Selinux Section
if [ "$DisableSelinux" == "YES" ]
then
	echo "Disabling Selinux"
	sed -ie 's/=enforcing/=permissive/g' /etc/sysconfig/selinux && setenforce 0
	if [ -f "/etc/selinux/config" ] # doesn't exist in all platforms
	then
		sed -ie 's/=enforcing/=permissive/g' /etc/selinux/config
	fi
        check_if_command_succeed
else
	echo "Disable Selinux is NOT Selected"
fi

# PFRING Support Section
if [ "$SupportPfring" == "YES" ]
then
	echo "Pfring Support is Selected"
	cd $Dir && wget -O $PfringVer.tar.gz $UrlPfring
	if [ $? -ne 0 ]
        then
                echo "Pfring Couldn't be downloaded !!"
		exit 1
	else
		tar xvfz $PfringVer.tar.gz && cd $Dir/$PfringVer/kernel && ./configure
		if [ $? -ne 0 ]
		then
			echo "Pfring COULD NOT BE ./configured !!!"
			exit 1
		else
			make && make install
			if [ $? -ne 0 ]
			then
				echo "Pfring/kernel installation has been FAILED !!!"
				echo "Maybe you have just compiled new kernel and still runs the Current One !!!"
				echo "You can try reboot then run the script Again !!!"
				exit 1
			else
				cd ../userland/lib && ./configure && make && make install
				if [ $? -ne 0 ]
	                        then
 	                               echo "Pfring/userland/lib installation has been FAILED !!!"
				       exit 1	
 	 		        else
				       modprobe pf_ring
				       echo "/usr/local/lib" > /etc/ld.so.conf.d/local.conf
				       ldconfig
				       cd ../../userland/libpcap && ./configure && make && make install
				       if [ $? -ne 0 ]
				       then
                                                echo "Pfring/usrland/libpcap installation has been FAILED !!!"
						exit 1
				       else
				       		echo "Installing Pfring Support OK "
						echo "
						#!/bin/sh
						# Auto Generated by InstallSuricata Script
		
						modprobe pf_ring enable_tx_capture=0 min_num_slots=16384
					        " > /etc/sysconfig/modules/pf_ring.modules
						chmod +x /etc/sysconfig/modules/pf_ring.modules && echo "Pfring Module config completed"	
				       fi
				fi
			fi
		fi

       fi
else
	echo "Support PFRING is NOT Selected"
fi

# Installation Section of Suricata
echo "Downloading Suricata"
cd $Dir && wget -O $SuricataVer.tar.gz $UrlSuricata

echo "Extracting Suricata"
tar xvfz $SuricataVer.tar.gz

echo "Installing HTP"
cd $Dir/$SuricataVer/libhtp && ./configure && make && make install
check_if_command_succeed

ldconfig || echo "ldconfig command COULD NOT worked as expected !!! "

echo "Installing Suricata"
cd $Dir/$SuricataVer && ./configure --enable-pfring --with-libpfring-libraries=/usr/local/lib --with-libpfring-includes=/usr/local/include --with-libpcaplibraries=/usr/local/lib --with-libpcap-includes=/usr/local/include LD_RUN_PATH="/usr/local/lib:/usr/lib:/usr/local/lib" 
if [ $? -ne 0 ]
then
	echo "Suricata COULD NOT BE ./configured !!!"
	exit 1
else
	make && make install && make install-full
	if [ $? -ne 0 ]
	then
		echo "Suricata installation has been FAILED !!!"
		exit 1
	else
		echo "Installing Suricata OK"
	fi
fi

# Configuration Section of Oinkmaster
echo "Installing Oinkmaster"
cd $Dir && wget -O $OinkVer.tar.gz $UrlOinkmaster

echo "Extracting Oinkmaster"
tar xvfz $OinkVer.tar.gz

echo "Installing Oinkmaster"
cd $Dir/$OinkVer/ && cp oinkmaster.pl /usr/local/bin/oinkmaster.pl

echo "Copying Oinkmaster Conf"
cd $Dir && cp $FilesDir/oinkmaster.conf oinkmaster.conf 
cp oinkmaster.conf /usr/local/etc/oinkmaster.conf

# Suricata YAML Configuration Section
echo "Downloading and customizing configuration files"

cd $Dir && cp $FilesDir/suricata.yaml suricata-$Interface.yaml

cp suricata-$Interface.yaml $Yaml

# Here we should examine accurate memory values based on
# Total memory which has been set in config file

sed -ie 's/(INTERFACE)/'$Interface'/g' $Yaml
sed -ie 's/(LOGFILE)/\/var\/log\/suricata-'$Interface'\//g' $Yaml
sed -ie 's/(PROCESSOR)/'$Processors'/g' $Yaml
sed -ie 's/(MEMINGB)/'$TotalMemory'/g' $Yaml
sed -ie 's/(MEMINKB)/'$((TotalMemory*1048576))'/g' $Yaml
sed -ie 's/(HALFMEMINGB)/'$((TotalMemory/2))'/g' $Yaml
sed -ie 's/(HALFMEMINKB)/'$((TotalMemory*524288))'/g' $Yaml
sed -ie 's/(23MEMINGB)/'$((TotalMemory*1))'/g' $Yaml
sed -ie 's/(23MEMINKB)/'$((TotalMemory*669050))'/g' $Yaml
sed -ie 's/(QUARTMEMINKB)/'$((TotalMemory*262144))'/g' $Yaml

# LSB Scripts of Suricata Service
echo "Copying template of LSB script and customizing"
cp $FilesDir/lsb.sh suricata-$Interface

cp suricata-$Interface $Lsb

sed -ie 's/(INTERFACE)/'$Interface'/g' $Lsb

echo "Making LSB script runnable"
chmod +x $Lsb

echo "Adding LSB script to system start-up"
chkconfig suricata-$Interface on

# Sysctl Tricks
echo "Sysctl Tricsk going on.."
grep #Added for Suricata /etc/sysctl.conf
if [ $? -ne 0 ]
then
	echo "
#Added for Suricata
net.core.netdev_max_backlog = 30000
net.core.rmem_default = 16777216
net.ipv4.tcp_no_metrics_save = 1
net.core.rmem_max=33554432
net.core.wmem_max=33554432
net.core.rmem_default=65536
net.core.wmem_default=65536
net.ipv4.tcp_rmem = 1048576 4194304 33554432
net.ipv4.tcp_wmem = 1048576 4194304 33554432
net.ipv4.route.flush=1
" >> /etc/sysctl.conf
else
	echo "Sysctl config is allready done before !!"
fi

# Scripts To Set Interface and Oinkmaster
echo "Creating directory for scripts"
mkdir -p $Script
check_if_command_succeed

echo "
#!/bin/bash
# Auto Created by InstallSuricata.sh Script

/sbin/ifconfig $Interface up
/sbin/ifconfig $Interface promisc
/sbin/ethtool -G $Interface rx 4096
for i in rx tx sg tso ufo gso gro lro; do /sbin/ethtool -K $Interface \$i off; done
" > $Script/setinterface.sh

echo "Changing permissions of the file"
chmod 755 $Script/setinterface.sh || echo "Couldn't change permissions for the file"

echo "
#!/bin/bash
# Auto Created by InstallSuricata.sh Script

PATH=/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/sbin:/usr/local/bin
/usr/local/bin/oinkmaster.pl -C /usr/local/etc/oinkmaster.conf -o /usr/local/etc/suricata/rules
/etc/init.d/suricata-$Interface restart
" > $Script/oinkmaster-cron.sh

echo "Changing permissions of the file"
chmod 755 $Script/oinkmaster-cron.sh || echo "Couldn't change permissions for the file"

echo "Adding scripts to cron.d"
echo "
@reboot root bash /scripts/setinterface.sh
00 4 * * * root bash /scripts/oinkmaster-cron.sh
" > /etc/cron.d/suricata

# This Sections About Configuration Of Log
echo "Creating log directory for $Interface interface"
mkdir -p $Log
check_if_command_succeed

echo "Adjusting rotation of logs"
echo "
$Log/eve.json {
        daily
	rotate 2
	copytruncate
	compress
	missingok
	notifempty
}
$Log/stats.log {
	daily
	rotate 2
	copytruncate
	compress
	missingok
	notifempty
}
" > /etc/logrotate.d/suricata

# Drop-rate Script For Analyzing Package 
echo "Installing & Configuring Drop Rate Script"
cd $Dir && cp $FilesDir/drop-rate.sh .

sed -ie 's/(INTERFACE)/'$Interface'/g' drop-rate.sh

# Set Line Number for Drop Rate Script
if [ "$Processors" == "4" ]
then
	sed -ie 's/(THREADS)/220/g' drop-rate.sh
elif [ "$Processors" == "8" ]
then
	sed -ie 's/(THREADS)/444/g' drop-rate.sh # Examine the correct line number for 8 threads
elif [ "$Processors" == "16" ]
then
	sed -ie 's/(THREADS)/884/g' drop-rate.sh
elif [ "$Processors" == "32" ]
then
	sed -ie 's/(THREADS)/1676/g' drop-rate.sh
fi

cp drop-rate.sh $Script/

chmod +x $Script/drop-rate.sh

$Script/setinterface.sh
	
echo "Starting the service .."
$Lsb start


