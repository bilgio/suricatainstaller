#!/bin/bash
#
# Init file for suricata
#
#
# chkconfig: 345 52 48
# description: Network Intrusion Detection System
#
# processname: Suricata-$INTERFACE
# pidfile: /var/run/suricata-eno16777736.pid

source /etc/rc.d/init.d/functions

### Variables

# It will parse the second interface (after lo) and set it to suricata proccess
# If you wan't suricata to listen another interface, you need to change the variable parser.
# ie: To parse 3rd interface (lo, eno...1, eno...2) set the grep value as "3: "

INTERFACE=$(ip a |egrep "2: " |awk '{print $2}' |tr -d ":")

### Read configuration
[ -r "$SYSCONFIG" ] && source "$SYSCONFIG"

RETVAL=0
prog="suricata-$INTERFACE"
desc="Suricata IDS on $INTERFACE"

start() {
    echo -n $"Starting $desc ($prog): "

    /usr/local/bin/suricata -D -c /usr/local/etc/suricata/suricata-$INTERFACE.yaml -i $INTERFACE --pfring --pidfile /var/run/suricata-$INTERFACE.pid
    RETVAL=$?
    echo
    [ $RETVAL -eq 0 ] && touch /var/lock/subsys/$prog
    return $RETVAL
}

stop() {
    echo -n $"Shutting down $desc ($prog): "
    killproc $prog
    RETVAL=$?
    echo
    [ $RETVAL -eq 0 ] && rm -f /var/lock/subsys/$prog
    return $RETVAL
}

restart() {
    stop
    start
}

case "$1" in
  start)
    start
    ;;
  stop)
    stop
    ;;
  restart)
    restart
    ;;
  reload)
    reload
    ;;
  condrestart)
    [ -e /var/lock/subsys/$prog ] && restart
    RETVAL=$?
    ;;
  status)
    status $prog
    RETVAL=$?
    ;;
  *)
    echo $"Usage: $0 {start|stop|restart|reload|condrestart|status}"
    RETVAL=1
esac

exit $RETVAL