#!/bin/bash
eth1pkts=$(tail -n (THREADS) /var/log/suricata-(INTERFACE)/stats.log |egrep kernel_packets |awk '{print $5}')
eth1drps=$(tail -n (THREADS) /var/log/suricata-(INTERFACE)/stats.log |egrep kernel_drops |awk '{print $5}')

i=0
for j in $eth1pkts
do
 i=`expr $i + $j`
done

totpkts=$i

k=0
for l in $eth1drps
do
 k=`expr $k + $l`
done

totdrps=$k
echo -n "(INTERFACE) "
echo -n "TotalPkgs: $totpkts - TotalDrops: $totdrps - Drop Rate: "
echo $totpkts $totdrps |awk '{print $2/$1 * 100}'

